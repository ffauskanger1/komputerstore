# Assignment 4: JavaScript Fundamentals

## Description

The assignment 4 was to create a "komputerstore".
It has been made with html, CSS and JS.
The CSS takes use of flexbox to have elements put in boxes.

## How to run

Use Live Server extension on HTML page to run the project from VS Code.

## Contributors
The contributors of this project is: Fredrik Fauskanger 
