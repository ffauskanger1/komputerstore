// Variables

const bankAccount = {
    balance: 0,
    loan: 0,
    pay: 0
}

let laptops = []
const nokFormat = new Intl.NumberFormat('no-NO', {style: 'currency', currency: 'NOK'})
const noroffURL = "https://noroff-komputer-store-api.herokuapp.com/"

// DOM Elements

// Paragraphs
const balanceElement = document.getElementById("balance")
const loanElement = document.getElementById("loan")
const payElement = document.getElementById('pay')
const laptopSelect = document.getElementById('laptops-select')
const laptopFeatures = document.getElementById('laptop-features')
const laptopTitle = document.getElementById("laptop-title")
const laptopDescription = document.getElementById("laptop-description")
const laptopPrice = document.getElementById("laptop-price")
const laptopImage = document.getElementById("laptop-image")

// Buttons
const loanBtn = document.getElementById("loan-btn")
const repayLoanBtn = document.getElementById("repay-loan-btn")
const bankBtn = document.getElementById("bank-btn")
const workBtn = document.getElementById("work-btn")
const buyPCBtn = document.getElementById("buypc-btn")

// Handlers
loanBtn.addEventListener('click', makeLoan)
workBtn.addEventListener('click', work)
bankBtn.addEventListener('click', bankMoney)
repayLoanBtn.addEventListener('click', repayLoan)
buyPCBtn.addEventListener('click', buyPC)

// Functions

// Initial setup of UI
renderBankAndWorkUI()

//Fetching and setting data
fetch(noroffURL + "computers")
.then((response) => response.json())
.then((data => laptops = data))
.then(laptops => addLaptopSelectOptions(laptops))
.catch(error => console.log(error))

/**
 * Adding options for all laptops and running rendering function for first laptop
 * @param laptops 
 */
const addLaptopSelectOptions = (laptops) => {
    laptops.forEach(laptop => addLaptopSelectOption(laptop))
    renderLaptopFeatures(laptops[0]) // Render first laptop
}

/**
 * Adding the options to the select menu
 * @param laptop 
 */
const addLaptopSelectOption = (laptop) =>
{
    const option = document.createElement('option');
    option.value = laptop.id;
    option.appendChild(document.createTextNode(laptop.title))
    laptopSelect.appendChild(option);
}


/**
 * Handles the laptop change, changing the features for the new laptop
 */
const handleLaptopChange = () => {
    const selectedLaptop = laptops[laptopSelect.selectedIndex];
    renderLaptopFeatures(selectedLaptop)
}

/**
 * Updating the information related to the laptop
 * @param laptop 
 */
const renderLaptopFeatures = (laptop) => 
{
    laptopFeatures.innerHTML = "";
    let specs = laptop.specs;
    specs.forEach(spec => {
        const feature = document.createElement("li")
        feature.innerText = spec;
        laptopFeatures.appendChild(feature);
    });

    laptopTitle.innerText = laptop.title
    laptopDescription.innerText = laptop.description
    laptopPrice.innerText = nokFormat.format(laptop.price)
    laptopImage.src = noroffURL + laptop.image
}

// Handle for change of laptop
laptopSelect.addEventListener('change', handleLaptopChange);

/**
 * Repays loan if there is enough money in the pay account.
 * Handles error if there is not enough money.
 */
function repayLoan()
{
    if(bankAccount.loan > bankAccount.pay) // If loan is higher than pay
    {
        alert("Not enough money to repay loan!")
    }
    else // Handle normally
    {
        bankAccount.pay -= bankAccount.loan

        if(bankAccount.pay > 0) // If any leftovers from pay, send to bank account
            bankAccount.balance += +bankAccount.pay

        bankAccount.loan = 0
        bankAccount.pay = 0
        repayLoanBtn.style.display = "none" // Hide repay button
        renderBankAndWorkUI()
    }
}

/**
 * Buys the PC that is currently selected.
 * Handles error if there is not enough money available from the balance.
 */
function buyPC()
{
    const selectedLaptop = laptops[laptopSelect.selectedIndex];
    console.log(selectedLaptop)
    if(selectedLaptop.price > bankAccount.balance) // If the price is higher than balance
    {
        alert("You do not have enough money!")
    }
    else // Remove the money and alert user
    {
        bankAccount.balance -= selectedLaptop.price 
        alert(`You are now the new owner of a nice ${selectedLaptop.title}!`)
        renderBankAndWorkUI()
    }
}

/**
 * Handles the banking of the money.
 * Two different ways of handling money, either with a loan or without.
 * Without loan it just transfers to the balance.
 * With loan it takes 10% away before transferring to the balance.
 */
function bankMoney()
{
    if(bankAccount.loan > 0) // Check if there is a loan
    {
        let moneyForLoan = bankAccount.pay * 0.1
        let moneyForBank = bankAccount.pay * 0.9
        if(moneyForLoan > bankAccount.loan) // If the 10% is higher than loan when banking
        {
            moneyForLoan -= bankAccount.loan
            bankAccount.balance += +moneyForLoan + +moneyForBank // Leftovers transferred to bank
            bankAccount.loan = 0
        }
        else // Handle normally
        {
            bankAccount.loan -= moneyForLoan
            bankAccount.balance += +moneyForBank // unary plus for handling number value
        }
        bankAccount.pay = 0
    }
    else // No loan
    {
        bankAccount.balance += +bankAccount.pay // unary plus for handling number value
        bankAccount.pay = 0
    }
    renderBankAndWorkUI()
}

/**
 * Working adds 100 to the money of the balance.
 */
function work()
{
    bankAccount.pay += +100 
    renderBankAndWorkUI()
}

/**
 * Prompts the user for a loan input.
 * Handles errors with the constraints of incorrect values.
 */
function makeLoan()
{
    const inputValue = prompt("Enter a loan")
    if(isNaN(inputValue) || inputValue <= 0 || bankAccount.loan > 0)
    {
        alert("Not a valid number, loan value or you have a current loan")
    }
    else if(inputValue > (bankAccount.balance * 2))
    {
        alert("Max loan value is: " + bankAccount.balance * 2)
    }
    else
    {
        bankAccount.loan = inputValue
        bankAccount.balance += +inputValue // unary plus for handling number value
        repayLoanBtn.style.display = "block" // Unhiding repay btn
        renderBankAndWorkUI() // Updating UI
    }
}

/**
 * Updates the UI for balance, loan and pay
 */
function renderBankAndWorkUI()
{
    balanceElement.innerHTML = `Balance: ${nokFormat.format(bankAccount.balance)}`
    loanElement.innerHTML = `Loan: ${nokFormat.format(bankAccount.loan)}`
    payElement.innerHTML = `Pay: ${nokFormat.format(bankAccount.pay)}`
}


